#!/bin/bash
source quit.sh
source help.sh
source ls.sh
source rm.sh
source rmd.sh
source about.sh
source version.sh
source age.sh
source cd.sh
source pwd.sh
source hour.sh
source open.sh
source httpget.sh
source log.sh
source profil.sh
source changepassw.sh

authenticate

cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | exit ) quit;;
    help ) show_help;;
    ls ) list_files;;
    rm ) rm_file @argv;;
    rmd ) rmdirec $argv;;
    about ) about_description;;
    version ) show_version;;
    age ) age_request;;
    cd ) change_directory;;
    pwd ) print_working_directory;;
    hour ) date_hour;;
    open ) open_file $argv;;
    httpget ) geturl;;
    profil ) show_profil;;
    passw ) change_password;;
    * ) echo "Commande inconnue. Tapez 'help pour obtenir de l'aide"
  esac
}

main() {
  lineCount=1

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mXzen\033[m ~ ☠️🥸 ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
