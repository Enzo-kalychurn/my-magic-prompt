
about_description() {
    echo "Le prompt 'My Magic Prompt' est un environnement de ligne de commande polyvalent qui vous permet d'interagir avec votre système Linux de manière conviviale. Il offre une gamme de commandes utiles, telles que la navigation dans les dossiers, la gestion de fichiers, la vérification de l'âge, l'affichage de l'heure, l'envoi d'e-mails, et bien plus encore. Vous pouvez également télécharger le code source HTML d'une page web et jouer à un jeu de Pierre-Papier-Ciseaux avec un ami. Le prompt est conçu pour être interactif et informatif, avec une aide détaillée pour chaque commande. Essayez-le pour simplifier vos tâches courantes en ligne de commande et explorer ses fonctionnalités magiques !"
    echo "version 1.0.0"
}