
change_password() {
    read -s -p "Entrez le mot de passe actuel: " old_password

    if [ "$old_password" = "$user_password" ]; then
        read -s -p "Entrez le nouveau mot de passe : " new_password
        user_password="$new_password"
        echo "Le mot de passe a été changé avec succès"
    else
        echo "Mot de passe incorrect. Le changement de mot de passe a échoué"
    fi
}
