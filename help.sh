
show_help() {
    echo "Commandes disponibles :
    help : Affiche cette aide.
    ls : Liste les fichiers et dossiers, y compris les cachés.
    rm : Supprime un fichier.
    rmd : Supprime un dossier.
    about : Affiche une description du programme.
    version : Affiche la version du prompt.
    age : Vous demande votre âge et vous indique si vous êtes majeur ou mineur.
    quit : Permet de sortir du prompt.
    profil : Affiche vos informations (prénom, nom, âge, email).
    passw : Changer le mot de passe avec confirmation.
    cd : Permet de naviguer dans les dossiers.
    pwd : Indique le répertoire actuel.
    hour : Affiche l'heure.
    * : Indique une commande inconnue.
    httpget : Télécharge le code source HTML d'une page web et l'enregistre dans un fichier spécifié.
    smtp : Vous permet d'envoyer un e-mail avec une adresse, un sujet et un corps de message.
    open : Ouvre un fichier dans l'éditeur VIM, même s'il n'existe pas."
}
