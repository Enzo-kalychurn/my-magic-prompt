
geturl() {
    read -p "Entrer l'url de la page à télécharger : " url
    read -p "Entrer le nom du fichier : " outfile

    curl -o "$outfile" "$url"

    if [ $? -eq 0 ]; then
        echo "Téléchargement réussi. Le code source est enregistré dans '$outfile'."
    else
        echo "Échec du téléchargement."
    fi
}