
change_directory() {
    read -p "Entrez le nom du répertoire : " directory

    if [ -d "$directory" ]; then
        cd "$directory"
        echo "Répertoire courant : $(pwd)"
    else
        echo "Le répertoire '$directory' n'existe pas."
    fi
}
