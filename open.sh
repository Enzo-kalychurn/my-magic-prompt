
open_file() {
    read -p "Entrer le nom du fichier à ouvrir " open
    if [[-e "$open"]]; then
        vim "$open"
    else
        touch "$open"
        vim "$open"
    fi
}