# My magic prompt

My Magic Prompt is a versatile command-line environment that allows you to interact with your Linux system in a user-friendly manner. It offers a range of useful commands, including folder navigation, file management, age verification, time display, email sending, and much more. You can also download the HTML source code of a web page. The prompt is designed to be interactive and informative, with detailed help for each command. Give it a try to simplify your common command-line tasks and explore its magical features!

## Available Commands

 ### help: 
 Display this help. 

### ls: 
List files and folders, including hidden ones. 

### rm: 
Delete a file. 

### rmd: 
Delete a folder. 

### about: 
Display a program description. 

### version: 
Display the prompt version. 

### age: 
Ask for your age and determine if you are of legal age or a minor. 

### quit: 
Exit the prompt. profile: Display your information (first name, last name, age, email). 

### passw: 
Change the password with confirmation. 

### cd: 
Navigate through folders. 

### pwd: 
Show the current directory. 

### hour: 
Display the time. 

### *: 
Indicates an unknown command. 

### httpget: 
Download the HTML source code of a web page and save it to a specified file. 

### open: 
Open a file in the VIM editor, even if it does not exist."
