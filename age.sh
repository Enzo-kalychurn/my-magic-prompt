
age_request() {
    read -p "Entrez votre âge : " age

    if [[ "$age" =~ ^[0-9]+$ ]]; then
        if [ "$age" -ge 18 ]; then
            echo "Vous êtes majeur."
        else
            echo "Vous êtes mineur."
        fi
    else
        echo "Age incorrect."
    fi
}